# springbootgcp-example

# Bitbucket Pipeline 
Pipeline installs the Springboot application in GCP app-engine standards environment.


# Pipeline Environment Variable
a. CLOUDSDK_CORE_PROJECT = Name of the GCP Project

b. GOOGLE_CLIENT_SECRET = JSON formatted key of Service Account with 3 roles (App Engine Admin, Datastore Index Admin, Storage Object Admin)